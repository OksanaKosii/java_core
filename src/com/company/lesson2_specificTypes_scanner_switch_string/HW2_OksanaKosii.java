package com.company.lesson2_specificTypes_scanner_switch_string;

import java.util.Scanner;

public class HW2_OksanaKosii {

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        System.out.println("Перше значення: ");
        double value1 = 0;
        double value2 = 0;
        char operator;

        if (scn.hasNextDouble()) {
            value1 = scn.nextDouble();
        } else {
            scn.next();
        }

        System.out.println("Дія: ");
        operator = scn.next().charAt(0);

        System.out.println("Друге значення: ");

        if (scn.hasNextDouble()) {
            value2 = scn.nextDouble();
        } else {
            scn.next();
        }

        switch (operator) {
            case '+': {
                System.out.println("Результат: " + (value1 + value2));
                break;
            }
            case '-': {
                System.out.println("Результат: " + (value1 - value2));
                break;
            }
            case '*': {
                System.out.println("Результат: " + (value1 * value2));
                break;
            }
            case '/': {
                if (value2 == 0) {
                    System.out.println("Операцію  виконати не можливо,тому що ділити на нуль не можна!");
                } else {
                    System.out.println("Результат: " + (value1 / value2));
                }
                break;
            }
            default: {
                System.out.println("error");
            }
        }
    }
}
