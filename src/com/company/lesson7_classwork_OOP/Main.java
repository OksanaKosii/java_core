package com.company.lesson7_classwork_OOP;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Person igor = new Person();
        Person andriy = new Person();

        igor.setAge(10);
        igor.setName("Igor");

        andriy.setAge(15);
        andriy.setName("Andriy");

        System.out.println(igor);

        igor.greeting();
        andriy.greeting();

        int igorAge = 10;
        String igorName = "Igor";

        int andriyAge = 15;
        String andriyName = "Andriy";

        System.out.println("Name = " + igorName);

        List<Person> people = new ArrayList<>();

        people.add(igor);
        people.add(andriy);

        for (Person person : people) {
            System.out.println(person);
        }

        System.out.println("igor age = " + igor.getAge());

        String str = null;

        if (str != null) {
            System.out.println(str);
        }

        String s = null;

        if (s != null) {
            igor.setName(s);
        }

        System.out.println(igor.getName().equals(andriy.getName()));

        Person nadya = new Person("Nadya", 20);
        System.out.println(nadya);
        Person sergiy = new Person("Sergiy");

    }
}
