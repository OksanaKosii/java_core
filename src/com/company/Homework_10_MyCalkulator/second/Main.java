package com.company.Homework_10_MyCalkulator.second;

public class Main {
    public static void main(String[] args) {
        MyCalculator calc = new MyCalculator(10.6, 4.5);

        MyCalculator.CalcResult(calc.plus());
        MyCalculator.CalcResult(calc.minus());
        MyCalculator.CalcResult(calc.multiply());
        MyCalculator.CalcResult(calc.divide());
    }

}
