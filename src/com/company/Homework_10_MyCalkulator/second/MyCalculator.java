package com.company.Homework_10_MyCalkulator.second;

import com.company.Homework_10_MyCalkulator.first.Numerable;

public class MyCalculator implements Numerable {

    private double x;
    private double y;

    public MyCalculator(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public double plus() {
        return x + y;
    }

    @Override
    public double minus() {
        return x - y;
    }

    @Override
    public double multiply() {
        return x * y;
    }

    @Override
    public double divide() {
        return x / y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    static void CalcResult(double result) {
        System.out.println("Результат " + result);
    }

    @Override
    public String toString() {
        return "MyCalculator{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
