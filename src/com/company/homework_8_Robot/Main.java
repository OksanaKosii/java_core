package com.company.homework_8_Robot;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.work();

        RobotCooker robotCooker = new RobotCooker();
        robotCooker.work();

        RobotDancer robotDancer = new RobotDancer();
        robotDancer.work();

        CoffeeRobot coffeeRobot = new CoffeeRobot();
        coffeeRobot.work();

        List<Robot> robots = new ArrayList<>();
        robots.add(robot);
        robots.add(robotCooker);
        robots.add(robotDancer);
        robots.add(coffeeRobot);

        for (Robot robot1 : robots){
            robot1.work();
        }
    }
}
