package com.company.Homework19_Set;

import java.util.Comparator;

public class NumberComparator implements Comparator<Purchase> {
    @Override
    public int compare(Purchase o1, Purchase o2) {
        return Integer.compare(o2.getNumber(), o1.getNumber());
    }
}
