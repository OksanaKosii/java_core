package com.company.Homework19_Set;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Purchase> list = new ArrayList<>();

        while (true) {
            System.out.println("Please enter a purchase name ");
            String name = scan.next();

            if (name.equalsIgnoreCase("end")) {
                break;
            }
            System.out.println("Please enter number ");
            int number = scan.nextInt();
            Purchase purchase = new Purchase(name, number);
            list.add(purchase);
        }

        System.out.println("В алфавітному порядку за ім’ям");
        Set<Purchase> sortByName = new TreeSet<>(list);
        System.out.println(sortByName);

        System.out.println("");

        System.out.println("В такому порядку як ви вводили");
        Set<Purchase> listShop = new LinkedHashSet<>(list);
        System.out.println(listShop);

        System.out.println();

        System.out.println("В порядку зменшення кількості");
        Set<Purchase> sortByNumber = new TreeSet<>(new NumberComparator());
        sortByNumber.addAll(list);
        System.out.println(sortByNumber);

        System.out.println();

        System.out.println("За хеш значенням об’єкта");
        Set<Purchase> hashSetList = new HashSet<>(list);
        System.out.println(hashSetList);
    }
}
