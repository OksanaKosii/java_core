package com.company.Homework19_Set;

import java.util.Objects;

public class Purchase implements Comparable<Purchase> {
    private String name;
    private int number;


    public Purchase(String name, int number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public int compareTo(Purchase o) {
        return this.name.compareTo(o.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Purchase purchase = (Purchase) o;
        return number == purchase.number && Objects.equals(name, purchase.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number);
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }
}
