package com.company.Homework_12.Instrument;

public class Drum implements InstrumentPlay {

    private int sizeDrum;

    public Drum(int sizeDrum) {
        this.sizeDrum = sizeDrum;
    }

    @Override
    public void play() {
        System.out.println("Plays a drum with a size of " + sizeDrum + "cm");
    }

    public int getSizeDrum() {
        return sizeDrum;
    }

    public void setSizeDrum(int sizeDrum) {
        this.sizeDrum = sizeDrum;
    }

    @Override
    public String toString() {
        return "Drum{" +
                "sizeDrum=" + sizeDrum +
                '}';
    }
}
