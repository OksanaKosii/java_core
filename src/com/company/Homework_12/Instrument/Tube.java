package com.company.Homework_12.Instrument;

public class Tube implements InstrumentPlay {

    private double diameterTube;

    public Tube(double diameterTube) {
        this.diameterTube = diameterTube;
    }

    @Override
    public void play() {
        System.out.println("Plays a tube with a diameter of " + diameterTube + "mm");
    }
    public double getDiameterTube() {
        return diameterTube;
    }

    public void setDiameterTube(double diameterTube) {
        this.diameterTube = diameterTube;
    }

    @Override
    public String toString() {
        return "Tube{" +
                "diameterTube=" + diameterTube +
                '}';
    }
}
