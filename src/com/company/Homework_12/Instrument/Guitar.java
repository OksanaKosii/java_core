package com.company.Homework_12.Instrument;

public class Guitar implements InstrumentPlay {

    private int strings;

    public Guitar(int strings) {
        this.strings = strings;
    }

    public int getStrings() {
        return strings;
    }

    public void setStrings(int strings) {
        this.strings = strings;
    }

    @Override
    public void play() {
        System.out.println("Plays a guitar with " + strings +" strings");

    }

    @Override
    public String toString() {
        return "Guitar{" +
                "strings=" + strings +
                '}';
    }
}
