package com.company.Homework_12.Instrument;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        InstrumentPlay guitar = new Guitar(6);
        InstrumentPlay drum = new Drum(22);
        InstrumentPlay tube = new Tube(5.2);

        List<InstrumentPlay> instrumentPlays = new ArrayList<>();
        instrumentPlays.add(guitar);
        instrumentPlays.add(drum);
        instrumentPlays.add(tube);

        for (InstrumentPlay instrumentPlay : instrumentPlays){
            instrumentPlay.play();
        }
    }
}
