package com.company.Homework_12.Celsius;

public class Fahrenheit implements Converter{

    @Override
    public double converters(double celsius) {
        return celsius * 1.8000 + 32.00;
    }
}
