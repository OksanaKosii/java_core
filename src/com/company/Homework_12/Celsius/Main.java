package com.company.Homework_12.Celsius;

import javax.xml.transform.Result;

public class Main {
    public static void main(String[] args) {
        Converter kelvin = new Kelvin();
        Converter fahrenheit = new Fahrenheit();

        System.out.println("Conversion from Celsius to Kelvin: " + kelvin.converters(32));
        System.out.println("Conversion from Celsius to Fahrenheit: " +fahrenheit.converters(15));

    }
}
