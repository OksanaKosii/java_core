package com.company.Homework_12.Celsius;

public class Kelvin implements Converter {

    @Override
    public double converters(double celsius) {
        return celsius + 273.15;
    }
}
