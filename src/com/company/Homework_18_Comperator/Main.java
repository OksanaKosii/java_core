package com.company.Homework_18_Comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        User ira = new User("Ira", 26, "ira@gmail.com");
        User mariya = new User("Marija", 20, "marija@yandex.ru");
        User vira = new User("Vira", 18, "vira@com");

        List<User> users = new ArrayList<>(
                List.of(ira, mariya, vira)
        );
        System.out.println(users);
        Collections.sort(users);
        System.out.println(users);
    }
}
