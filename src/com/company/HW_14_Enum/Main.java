package com.company.HW_14_Enum;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter any month of year: ");

        String scanMonth = sc.nextLine();

        Months valueMonths = Months.valueOf(scanMonth.toUpperCase());

        for (Months months : Months.values()) {

            if (months.name().equals(valueMonths.toString())) {
                System.out.println("This month exists.");
            }
        }
        System.out.println("Months with the same time of year: ");

        switch (valueMonths.getSeasons()) {
            case WINTER:{
                System.out.println(Months.DECEMBER);
                System.out.println(Months.JANUARY);
                System.out.println(Months.FEBRUARY);
            }break;
            case SPRING: {
                System.out.println(Months.MARCH);
                System.out.println(Months.APRIL);
                System.out.println(Months.MAY);
            }break;
            case SUMMER: {
                System.out.println(Months.JUNE);
                System.out.println(Months.JULY);
                System.out.println(Months.AUGUST);
            }break;
            case AUTUMN: {
                System.out.println(Months.SEPTEMBER);
                System.out.println(Months.OCTOBER);
                System.out.println(Months.NOVEMBER);
            }
        }
        System.out.println("Months that have the same number of days: " + valueMonths.getDays());

        for (Months months : Months.values()) {
            if (valueMonths.getDays() == months.getDays()) {
                System.out.println( months + " " + months.getDays());
            }
        }

        System.out.println("Months with fewer days: " + valueMonths.getDays());

        for (Months months : Months.values()) {
            if (valueMonths.getDays() > months.getDays()) {
                System.out.println( months + ":" + months.getDays());
            }
        }

        System.out.println("Months with more days: " + valueMonths.getDays());

        for (Months months : Months.values()) {
            if (valueMonths.getDays() < months.getDays()) {
                System.out.println( months + ":" + months.getDays());
            }
        }
        System.out.println( "The next season: ");

        switch (valueMonths.getSeasons()) {
            case WINTER: {
                System.out.println(Seasons.SPRING);
            }break;
            case SPRING: {
                System.out.println(Seasons.SUMMER);
            }break;
            case SUMMER: {
                System.out.println(Seasons.AUTUMN);
            }break;
            case AUTUMN: {
                System.out.println(Seasons.WINTER);
            }break;
        }

        System.out.println("Previous season: ");

        switch (valueMonths.getSeasons()){
            case WINTER: {
                System.out.println(Seasons.AUTUMN);
            }break;
            case SPRING: {
                System.out.println(Seasons.WINTER);
            }break;
            case SUMMER: {
                System.out.println(Seasons.SPRING);
            }break;
            case AUTUMN: {
                System.out.println(Seasons.SUMMER);
            }break;
        }
        System.out.println("Month with an even number of days: " + valueMonths.getDays());

        for (Months months : Months.values()) {

            if (months.getDays() % 2 == 0) {
                System.out.println(months + " " + months.getDays());
            }
        }

        System.out.println("A month with an odd number of days: ");

            for (Months months : Months.values()) {

                if (months.getDays() % 2 != 0) {
                    System.out.println(months + " " + months.getDays());
                }
            }

            if (valueMonths.getDays() % 2 == 0  ) {
                System.out.println( valueMonths + " has an even number of days: " + valueMonths.getDays());

            } else  {
                System.out.println( valueMonths + " doesn't have an even number of days: " + valueMonths.getDays());
        }
    }
}



