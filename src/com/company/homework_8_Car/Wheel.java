package com.company.homework_8_Car;

public class Wheel {
    private double wheelSize;

    public Wheel(double wheelSize) {
        this.wheelSize = wheelSize;
    }

    public void wheelSizeEnlarge() {
        this.wheelSize *=10 ;

    }

    public double getWheelSize() {
        return wheelSize;
    }

    public void setWheelSize(double wheelSize) {
        this.wheelSize = wheelSize;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "wheelSize=" + wheelSize +
                '}';
    }
}
