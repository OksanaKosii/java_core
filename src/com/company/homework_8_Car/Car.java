package com.company.homework_8_Car;

public class Car {
    private int enginePower;
    private int numberDoors;
    private SteeringWheel steeringWheel;
    private Wheel wheel;
    private Body body;

    public Car(int enginePower, int numberOfDoors, SteeringWheel steeringWheel, Wheel wheel, Body body) {
        this.enginePower = enginePower;
        this.numberDoors = numberOfDoors;
        this.steeringWheel = steeringWheel;
        this.wheel = wheel;
        this.body = body;
    }

    public void changeNumberDoors() {
        this.numberDoors -= 2;
    }

    public void newEnginePower() {
        this.enginePower *= 3;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public int getNumberDoors() {
        return numberDoors;
    }

    public void setNumberDoors(int numberDoors) {
        this.numberDoors = numberDoors;
    }

    public SteeringWheel getSteeringWheel() {
        return steeringWheel;
    }

    public void setSteeringWheel(SteeringWheel steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Car{" +
                "enginePower=" + enginePower +
                ", numberDoors=" + numberDoors +
                ", steeringWheel=" + steeringWheel +
                ", wheel=" + wheel +
                ", body=" + body +
                '}';
    }
}
