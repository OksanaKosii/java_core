package com.company.homework_8_Car;

public class SteeringWheel {

    private double diameter;

    public SteeringWheel(double diameter) {
        this.diameter = diameter;
    }

    public void changeDiameter() {
        this.diameter /=  2;

    }
    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return "SteeringWheel{" +
                "diameter=" + diameter +
                '}';
    }
}
