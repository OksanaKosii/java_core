package com.company.lesson3_methods_cycles;

import java.util.Scanner;

public class HW3_OksanaKosii {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // Виводить на екран невід'ємні елементи
        for (int i = 90; i >= 0; i -= 5) {
            System.out.println(i);
        }
        // Найблище до 10 з 2 чисел
        System.out.println("Ввести 2 значення");
        double m = sc.nextDouble();
        double n = sc.nextDouble();
        int a = 10;

        if (Math.abs(a - m) < Math.abs(a - n)) {
            System.out.println(m);
        } else if (Math.abs(a - m) == Math.abs(a - n)) {
            System.out.println(m + " " + n);
        } else {
            System.out.println(n);
        }

        //  Task 1
        System.out.println("Example '8 * 9 = ?' ");
        int out = 8 * 9;
        double k;

        do {
            System.out.println("Your answer: ");
            k = sc.nextDouble();
        } while (k != out);
        System.out.println("The answer is correct ");

        // Task 2
        String rightPass = "pass123";
        String admin = "admin";
        System.out.println("Enter the password");

        while (true) {
            String pass = sc.next();

            if (rightPass.equals(pass)) {
                System.out.println("The password is correct");
                break;
            } else if (admin.equals(pass)) {
                System.out.println("You are admin");
                break;
            } else {
                System.out.println("Incorrect password try again");
            }
        }
    }
}
