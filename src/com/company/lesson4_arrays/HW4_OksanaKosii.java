package com.company.lesson4_arrays;

public class  HW4_OksanaKosii {
    public static void main(String[] args) {

        // Task 1
        System.out.println("Task 1");
        int[] arr1 = new int[100];

        for (int i = 0; i < arr1.length; ++i) {
            arr1[i] = i + 1;

            if (arr1[i] % 14 == 0) {
                System.out.println(arr1[i]);
            }
        }
        //Task2
        System.out.println("Task 2");
        int[] arr2 = {4, 1, 5, 8, 1, 4, 2, 2, 3, 4, 6, 7, 5};
        int[] array = new int[100];
        int size = 0;

        for (int i = 0; i < arr2.length; ++i) {
            for (int j = i + 1; j < arr2.length; ++j) {
                if (arr2[i] == arr2[j]) {
                    array[size] = arr2[i];
                    break;
                }
            }
        }

        //Task 3
        System.out.println("Task 3");
        int[] arr3 = new int[10];
        arr3[0] = 0;
        arr3[1] = 1;

        for (int i = 2; i < arr3.length; ++i) {
            arr3[i] = arr3[i - 1] + arr3[i - 2];
        }

        for (int i = 0; i < arr3.length; ++i) {
            System.out.println(arr3[i]);
        }

        //Task 4
        System.out.println("Task 4");
        int[] arr4 = {5, 8, 4, 2, 12, 5, 6, 25, 7, 5, 9, 6, 13, 4, 3};
        int max = arr4[0];
        int min = arr4[0];
        int maxIndex = 0;
        int minIndex = 0;

        for (int k = 0; k < arr4.length; ++k) {

            if (arr4[k] > max) {
                max = arr4[k];
                maxIndex = k;
            }

            if (arr4[k] < min) {
                min = arr4[k];
                minIndex = k;
            }
        }
        System.out.println("Індекс максимального значення: " + maxIndex);
        System.out.println("Індекс мінімального значення: " + minIndex);

        //Task 5
        System.out.println("Task 5");
        int[] array1 = {4, 1, 3, 8, 9, 11, 6, 8, 4, 12};
        int[] array2 = {2, 4, 1, 6, 10, 5, 4, 7, 3, 5};
        int[] array3 = new int[10];

        for (int i = 0; i < array1.length; ++i) {
            array3[i] = array1[i] - array2[i];
            System.out.println(array3[i]);
        }

        //Task 6
        System.out.println("Task 6");
        int[] arr6 = {5, 8, 4, 3, 12, 5, 6, 1, 2, 7, 5, 9, 6, 25, 13, 45, 32};

        for (int i = 0; i < arr6.length; ++i) {
            for (int j = 0; j < arr6.length; ++j) {

                if (arr6[i] < arr6[j]) {
                    int k = arr6[i];
                    arr6[i] = arr6[j];
                    arr6[j] = k;
                }
            }
        }

        for (int i = 0; i < arr6.length; ++i) {
            System.out.println(arr6[i]);
        }
    }

}
