package com.company.lesson4_arrays;

import java.util.Scanner;

public class Classwork {
    public static void main(String[] args) {

        int[] array = {10,25,33,11,55};

        int[] array2 = new int[1000];

        int size = 10;

        int[] array3 = new int[size];

        System.out.println(array[1]);

//        array[1] = 10;

        System.out.println("Length = " + array.length);
        System.out.println("Length = " + array2.length);

        for (int i = 0; i < array.length; ++i) {
            System.out.println(array[i]);
        }

        array2[0] = 10;

        for (int i = 0; i < array2.length; ++i) {
            System.out.println(array2[i]);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter size of array:");
        int length = scanner.nextInt();

        int[] arr = new int[length];

        for (int i = 0; i < arr.length; ++i) {
            System.out.println("Enter number: ");
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < arr.length; ++i) {
            System.out.println(arr[i]);
        }
    }
}
