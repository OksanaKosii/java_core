package com.company.lesson6_wrappers_git;

import java.util.ArrayList;
import java.util.List;

public class Classwork {
    public static void main(String[] args) {

        int i = 0;
        Integer j = 0;

        double d = j.doubleValue();
        d = j;

        String[] array = {"word1", "word2", "word3"};

//        array[3] = "word4";

        List<String> list = new ArrayList<>();

        list.add("word1"); // Додає елемент
        list.size(); // Кількість елементів
        System.out.println(list.get(0)); // Бере елемент по індексу

        list.add("word2");

        System.out.println(list);
        System.out.println(array);

        System.out.println(list.isEmpty()); //true якщо пустий, false якщо містить елементи
        list.remove(0); // Видаляє по індексу
        list.remove("word2");// Видаляє по значенню
        System.out.println(list.isEmpty());
        System.out.println(list);

        List<Integer> numbers = new ArrayList<>();
    }
}

