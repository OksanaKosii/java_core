package com.company.homework7_Rectangle;

public class Main {
    public static void main(String[] args) {

        Rectangle rectangle1 = new Rectangle();

        rectangle1.setHeight(5);
        rectangle1.setWidth(4.5);

        Rectangle rectangle2 = new Rectangle(5.2, 3.5);

        System.out.println("Площа прямокутника = " + rectangle1.getArea());
        System.out.println("Периметр прямокутника = " + rectangle1.getPerimeter());
        System.out.println("Площа прямокутника = " + rectangle2.getArea());
        System.out.println("Периметр прямокутника = " + rectangle2.getPerimeter());
    }
}
