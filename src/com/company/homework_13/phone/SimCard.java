package com.company.homework_13.phone;

import java.util.List;

public class SimCard  {

    private String operator;
    private int number;

    public SimCard(String operator, int number) {
        this.operator = operator;
        this.number = number;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "SimCard{" +
                "operator='" + operator + '\'' +
                ", number=" + number +
                '}';
    }
}
