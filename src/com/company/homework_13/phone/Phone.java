package com.company.homework_13.phone;

import java.util.List;
import java.util.Objects;

public class Phone {

    private SimCard simCard;
    private String model;
    private double weight;

    public Phone() {
    }

    public Phone(SimCard simCard, String model, double weight) {
        this(simCard, model);
        this.weight = weight;
    }

    public Phone(SimCard simCard, String model) {
        this.simCard = simCard;
        this.model = model;
    }

    public void receiveCall(int number) {
        System.out.println("An unknown number is calling: " + number);
    }

    public void receiveCall(String name, int number) {
        System.out.println("Calling is " + name + " with number " + number);
    }

    public void  sendMessage(String message, List<Integer> number) {
        System.out.println("Message: " + message + " is sending to: " + number);
    }

    public SimCard getSimCard() {
        return simCard;
    }

    public void setSimCard(SimCard simCard) {
        this.simCard = simCard;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Double.compare(phone.weight, weight) == 0 && Objects.equals(simCard, phone.simCard) && Objects.equals(model, phone.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(simCard, model, weight);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "simCard=" + simCard +
                ", model='" + model + '\'' +
                ", weight=" + weight +
                '}';
    }

}
