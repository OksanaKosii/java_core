package com.company.homework_13.phone;


import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Phone phone1 = new Phone(new SimCard("MTS", 502412545), "Iphone", 300);
        Phone phone2 = new Phone(new SimCard("life", 502401557), "Nokia", 200);
        Phone phone3 = new Phone(new SimCard("Kyivstar", 952246284), "Samsung", 250);

        System.out.println(phone1);
        System.out.println(phone2);
        System.out.println(phone3);

        phone1.receiveCall(675544544);
        phone2.receiveCall(508746512);
        phone3.receiveCall(687587426);

        Phone phone = new Phone(new SimCard("MTS", 980457545), "Iphone");

        phone.receiveCall("Andriy", 954874554);
        phone1.receiveCall("Ihor", 504865556);
        phone2.receiveCall("Ira", 502123654);
        phone3.receiveCall("Oleg", 687457451);

        List<Integer> number = new ArrayList<>();
        number.add(502504667);
        number.add(952468514);
        number.add(952484625);
        number.add(502456855);

        phone.sendMessage("'Congratulations'", number);

    }

}

