package com.company.homework_13.person;

public class Main {
    public static void main(String[] args) {

        Person person1 = new Person();
        Person person2 = new Person("Ira",25);

        person1.setFullName("Ihor");
        person1.setAge(31);

        person1.movie();
        person1.talk();
        person2.movie();
        person2.talk();

    }
}
