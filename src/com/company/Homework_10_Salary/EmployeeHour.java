package com.company.Homework_10_Salary;

public class EmployeeHour implements Salary {
    private String name;
    private int hourRate;
    private double workedHours;

    public EmployeeHour(String name, int hourRate, double workedHours) {
        this.name = name;
        this.hourRate = hourRate;
        this.workedHours = workedHours;
    }

    @Override
    public void salary() {
        double salary = hourRate * workedHours;
        System.out.println("Зарплата працівника " + getName() + " становить " + salary + " грн.");

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHourRate() {
        return hourRate;
    }

    public void setHourRate(int hourRate) {
        this.hourRate = hourRate;
    }

    public double getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(double workedHours) {
        this.workedHours = workedHours;
    }

    @Override
    public String toString() {
        return "EmployeeHour{" +
                "name='" + name + '\'' +
                ", hourRate=" + hourRate +
                ", workedHours=" + workedHours +
                '}';
    }
}
