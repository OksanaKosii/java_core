package com.company.Homework_10_Salary;

public class Main {
    public static void main(String[] args) {
        EmployeeHour employeeHour = new EmployeeHour("Mariya", 175, 41.25);
        EmployeeMonth employeeMonth = new EmployeeMonth("Andriy", 24.5, 315);

        employeeHour.salary();
        employeeMonth.salary();
    }
}
