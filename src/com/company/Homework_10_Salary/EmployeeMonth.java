package com.company.Homework_10_Salary;

public class EmployeeMonth implements Salary {
    private String name;
    private double workedDay;
    private double dayRate;

    public EmployeeMonth(String name, double workedDay, double dayRate) {
        this.name = name;
        this.workedDay = workedDay;
        this.dayRate = dayRate;
    }

    @Override
    public void salary() {
        double salary = dayRate * workedDay;
        System.out.println("Зарплата працівника " + getName() + "  становить " + salary + " грн.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWorkedDay() {
        return workedDay;
    }

    public void setWorkedDay(double workedDay) {
        this.workedDay = workedDay;
    }

    public double getDayRate() {
        return dayRate;
    }

    public void setDayRate(double dayRate) {
        this.dayRate = dayRate;
    }
}
