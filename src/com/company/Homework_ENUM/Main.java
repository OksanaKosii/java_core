package com.company.Homework_ENUM;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Enter any time of year: ");
        String scanSeason = sc.nextLine();

        Season season = Season.valueOf(scanSeason);

        switch (season) {
            case WINTER: {
                System.out.println("It's snowing");
            }
            break;
            case SPRING: {
                System.out.println("Green grass");
            }
            break;
            case SUMMER: {
                System.out.println("The sun shines");
            }
            break;
            case AUTUMN: {
                System.out.println("The leaves fall");
            }
        }


        for (Season value : Season.values()) {
            System.out.println(value);
        }

        System.out.println(Season.WINTER.ordinal());
        System.out.println(Season.SPRING.ordinal());
        System.out.println(Season.SUMMER.ordinal());
        System.out.println(Season.AUTUMN.ordinal());
    }
}
