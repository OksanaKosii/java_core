package com.company;

import java.util.Scanner;

public class H_W_1_OksanaKosii {

    public static void main(String[] args) {
        // Завдання 1

        Scanner s = new Scanner(System.in);
        double a = s.nextDouble();
        double b = s.nextDouble();
        double c = s.nextDouble();

//        double a = 2;
//        double b = -5;
//        double c = 3;
        double d = b * b - 4 * a * c;
        if (d > 0) {
            double x1 = (-b + Math.sqrt(d)) / (2 * a);
            double x2 = (-b - Math.sqrt(d)) / (2 * a);
            System.out.println(" x1 = " + x1 + ", x2 = " + x2);
        } else if (d == 0) {
            double x = -b / (2 * a);
            System.out.println(" x = " + x);
        } else {
            System.out.println("Розв'язків немає");
        }

// Завдання 2
        int x3 = 8;
        int y1;
        if (x3 < 10) {
            y1 = x3 + 9;
            System.out.println(y1);
        } else if (x3 > 15) {
            y1 = x3 - 5;
            System.out.println(y1);
        } else {
            y1 = x3 * x3;
            System.out.println(y1);
        }

// Завдання 3
        double a1 = 10;
        double b1 = 5;
        double c1 = 11;
        double d1 = 6;
        double res = a1 + b1 * (c1 - d1) / 2;
        System.out.println(res);
    }
}
