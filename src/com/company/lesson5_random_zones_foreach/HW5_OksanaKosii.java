package com.company.lesson5_random_zones_foreach;

import java.util.Scanner;
import java.util.Arrays;

public class HW5_OksanaKosii {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        final String firstChoice = "X";
        final String secondChoice = "O";
        String[] arr = {"*", "*", "*", "*", "*", "*", "*", "*", "*",};

        printWelcome();
        print(arr);

        while (true) {

            System.out.println("Перший гравець вводить індекс поля:");

            arr[InputIndexesCheck(arr, sc)] = firstChoice;

            print(arr);

            if (checkResultGame(arr, firstChoice)){
                break;
            }

            System.out.println("Другий гравець вводить індекс поля:");

            arr[InputIndexesCheck(arr, sc)] = secondChoice;

            if (checkResultGame(arr, secondChoice)){
                break;
            }

            print(arr);
        }
    }

    public static void printWelcome() {
        System.out.println();
        System.out.println("Привіт!");
        System.out.println("Ласкаво просимо до гри 'Хрестики - нулики'!");
        System.out.println("Почніть гру!");
        System.out.println();
    }

    public static void print(String[] InputArr) {
        System.out.println(InputArr[0] + " | " + InputArr[1] + " | " + InputArr[2]);
        System.out.println("-- --- ---");
        System.out.println(InputArr[3] + " | " + InputArr[4] + " | " + InputArr[5]);
        System.out.println("-- --- ---");
        System.out.println(InputArr[6] + " | " + InputArr[7] + " | " + InputArr[8]);
    }

    public static int InputIndexesCheck(String[] InputArr, Scanner sc) {
        int inputIndex = sc.nextInt();

        while ((inputIndex > 8) || (inputIndex < 0) || (!InputArr[inputIndex].equals("*"))) {
            System.out.println("Індекс некоректний або інший гравець його уже ввів , введіть інший: ");
            inputIndex = sc.nextInt();
        }
        return inputIndex;
    }

    public static boolean checkResultGame(String[] InputArr, String indexPlayer) {

        if (
            (InputArr[0].equals(indexPlayer) && InputArr[3].equals(indexPlayer) && InputArr[6].equals(indexPlayer)) ||
            (InputArr[1].equals(indexPlayer) && InputArr[4].equals(indexPlayer) && InputArr[7].equals(indexPlayer)) ||
            (InputArr[2].equals(indexPlayer) && InputArr[5].equals(indexPlayer) && InputArr[8].equals(indexPlayer)) ||
            (InputArr[0].equals(indexPlayer) && InputArr[1].equals(indexPlayer) && InputArr[2].equals(indexPlayer)) ||
            (InputArr[3].equals(indexPlayer) && InputArr[4].equals(indexPlayer) && InputArr[5].equals(indexPlayer)) ||
            (InputArr[6].equals(indexPlayer) && InputArr[7].equals(indexPlayer) && InputArr[8].equals(indexPlayer)) ||
            (InputArr[0].equals(indexPlayer) && InputArr[4].equals(indexPlayer) && InputArr[8].equals(indexPlayer)) ||
            (InputArr[2].equals(indexPlayer) && InputArr[4].equals(indexPlayer) && InputArr[6].equals(indexPlayer))
        ) {
            System.out.println("Гру закінчено. Ви виграли!");
            print(InputArr);
            return true;

        } else if (!Arrays.asList(InputArr).contains("*")) {
            System.out.println("Гру закінчено. Нічия!");
            print(InputArr);
            return true;
        }
        {
            return false;
        }
    }
 }
