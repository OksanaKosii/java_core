package com.company.lesson5_random_zones_foreach;

import java.util.Random;

public class Classwork {
    public static void main(String[] args) {
        String[] seasons = {"Winter", "Spring", "Summer", "Autumn"};

        int a = 10;

        for (String season: seasons) {
            System.out.println(season);
            season = "abc";
        }

        for (String season: seasons) {
            System.out.println(season);
        }
        int x = 5;
        System.out.println(x);

        m(x);
        System.out.println(x);
        Random random = new Random();
         int min = -10;
         int max = 11;
         int diff = max - min;

        for (int i = 0; i < 10; ++i) {
            System.out.println(random.nextInt(diff) + min);
        }

        int min1 = -50;
        int max2 = 150;
        int diff2 = max2 - min1;
        int[] arr = new int[10];
        for ( int i = 0; i < arr.length; ++i) {
            arr[i] = random.nextInt(diff2) + min1;
        }
        for (int elem : arr) {
            System.out.println(arr[elem]);
        }
    }
    public static void m(int a) {
        a = 10;
    }
}
